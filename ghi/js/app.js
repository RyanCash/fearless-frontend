function createCard(
  name,
  description,
  pictureUrl, 
  start_date,
  end_date,
  location,
) {
  const formattedStartDate = new Date(start_date).toLocaleDateString();
  const formattedEndDate = new Date(end_date).toLocaleDateString();
  return `
    <div class="col-lg-4">
      <div class="card shadow-sm border-0 mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          ${formattedStartDate} - ${formattedEndDate}
        </div>
      </div>
    </div>

`;
}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Something went wrong');
    } else {
      const data = await response.json();

      const column = document.querySelector('.row');
      column.innerHTML = '';

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start_date = details.conference.start_date;
          const end_date = details.conference.end_date;
          const location = details.conference.location.name;
          const html = createCard(
            title,
            description,
            pictureUrl,
            start_date,
            end_date,
            location
            );
            console.log(details)
          //const column = document.querySelector('.col');
          column.innerHTML += html;
        }

      // const conference = data.conferences[0];
      // console.log(conference);

      // const nameTag = document.querySelector('.card-title');
      // nameTag.innerHTML = conference.name;

    }
  }

  } catch (error) {
    console.error('Error: Something went wrong');
  }

});








  


